#include<stdio.h>  
  int main(void)  
{  
    const float PI = 3.14;  
          float r, h, volume;  
  
    printf("Enter the height of the cone:\n");  
    scanf("%f", &h);
    printf("Enter the radius of the cone:\n");  
    scanf("%f", &r);  
  
    volume = (PI * r * r * h) / 3;  
  
    printf("Volume of the Cone is %f\n", volume);  
  
    return 0;  
}  