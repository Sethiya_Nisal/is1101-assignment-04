#include<stdio.h>
 int main(void)
 {
     float num1, num2, num3;
     float sum;
     float avg;

     printf("Enter the 1st Number:\n");
     scanf("%f", &num1);
     printf("Enter the 2nd Number:\n");
     scanf("%f", &num2);
     printf("Enter the 3rd Number:\n");
     scanf("%f", &num3);

     sum = num1 + num2 + num3;

     avg = sum / 3;

     printf("Sum of the three numbers=%.2f\n", sum);
     printf("Average=%.2f\n", avg);

     return 0;
}